%include "src/colon.inc"
%include "src/lib.inc"
%include "src/dict.inc"
%include "src/words.inc"

global _start

%define BUFF_SIZE 256
%define PTR_SIZE 8

section .rodata
prompt_msg: db 'Enter key: ', 0
input_fail_msg: db 'Key length must be between 0 and 256', 0
nothing_match_msg: db 'Nothing matches provided key', 0
key_match_msg: db 'Found matching key: ', 0
separator_msg: db ' - ', 0

section .bss
input_buffer: resb BUFF_SIZE

section .text
_start:
    mov rdi, prompt_msg
    call print_string

.read_input:
    mov rdi, input_buffer
    mov rsi, BUFF_SIZE
    call read_word
    test rax, rax
    jz .input_fail

    push rdx 

.check_input:
    mov rdi, input_buffer 
    mov rsi, NEXT 
    call find_word
    test rax, rax
    jz .nothing_match

.print_match:
    mov rdi, key_match_msg
    push rax
    call print_string
    pop rax

    add rax, PTR_SIZE 
    mov rdi, rax
    push rdi
    call print_string

    mov rdi, separator_msg
    call print_string
    pop rdi

    pop rdx 
    lea rdi, [rdi + rdx + 1] 
    call print_string
    call print_newline
    xor rdi, rdi
    call exit

.input_fail:
    mov rdi, input_fail_msg
    call print_string_err
    call print_newline_err
    mov rdi, 1
    call exit

.nothing_match:
    mov rdi, nothing_match_msg
    call print_string_err
    call print_newline_err
    mov rdi, 1
    call exit
