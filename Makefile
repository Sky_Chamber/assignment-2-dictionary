AS=nasm
ASFLAGS=-f elf64 -g
LD=ld

SRC_DIR=src
OBJ_DIR=obj
EXECUTABLE=main

SOURCES := $(wildcard $(SRC_DIR)/*.asm)
OBJECTS := $(SOURCES:$(SRC_DIR)/%.asm=$(OBJ_DIR)/%.o)

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(LD) $(OBJECTS) -o $@

$(OBJECTS): $(OBJ_DIR)/%.o : $(SRC_DIR)/%.asm
	@mkdir -p $(@D)
	$(AS) $(ASFLAGS) $< -o $@

clean:
	rm -rf $(OBJ_DIR)/*.o $(EXECUTABLE)