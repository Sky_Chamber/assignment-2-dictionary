%include "src/lib.inc"

global find_word

%define PTR_SIZE 8
%define NULL 0

section .text

find_word:

xor rax, rax 
mov rdx, rsi 

.loop:
	add rsi, 8 

	push rdi
	push rdx
	call string_equals 
	pop rdx
	pop rdi
	cmp rax, 1 
	je .found 
	mov rsi, [rdx] 
	mov rdx, rsi 
	test rdx, rdx 
	je .not_found 
	jmp .loop 

.found:
	mov rax, rdx 
	ret
.not_found:
	xor rax, rax 
	ret
