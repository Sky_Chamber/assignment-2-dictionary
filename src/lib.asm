global exit
global parse_int
global parse_uint
global print_char
global print_int
global print_newline
global print_newline_err
global print_string
global print_string_err
global print_uint
global read_char
global read_word
global string_copy
global string_equals
global string_length

%define STDIN 0
%define STDOUT 1
%define STDERR 2
%define SYSCALL_READ  0
%define SYSCALL_WRITE 1
%define SYSCALL_EXIT  60
%define NULL_CHAR 0
%define ASCII_MINUS 45

section .text

exit: 
    mov rax, SYSCALL_EXIT
    syscall

string_length:
    xor rax, rax
    .loop:
        cmp BYTE [rdi+rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

print_string_err:
    mov r9, STDERR
    jmp _print_string

print_string:
    mov r9, STDOUT

_print_string:
    push r9
    push rdi
    call string_length
    pop rdi
    pop r9

echo:
    mov rsi, rdi
    mov rdx, rax
    mov rax, SYSCALL_WRITE
    mov rdi, r9
    syscall
    ret


print_char:
    mov r9, STDOUT
    jmp _print_char

print_char_err:
    mov r9, STDERR

_print_char:
    push rdi
    mov rdi, rsp
    mov rax, 1
    call echo
    pop rdi 
    ret

print_newline_err:
    mov rdi, 0xA
    jmp print_char_err


print_newline:
    mov rdi, 0xA
    jmp print_char


print_uint:
    xor rax, rax 
    xor rdx, rdx 
    mov rsi, rsp 
    mov rcx, 10 
    mov rax, rdi 
    push 0 
.loop:
    xor rdx, rdx 
    div rcx 
    add dl, 0x30 
    dec rsp 
    mov [rsp], dl 
    test rax, rax 
    jnz .loop 
    mov rdi, rsp 
    push rsi 
    call print_string 
    pop rsi 
    mov rsp, rsi 
    ret


print_int:
    test rdi, rdi
    jns print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint


string_equals:
    xor rax, rax
    xor rcx, rcx

.compare_chars:
    mov byte dl, byte [rdi+rcx]
    cmp byte dl, byte [rsi+rcx]
    jne .end
    inc rcx
    cmp byte dl, NULL_CHAR
    jne .compare_chars
    inc rax
.end:
    ret


read_char:
    xor rax, rax
    xor rdi, rdi
    push 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret


read_word:
    xor rdx, rdx
    dec rsi

.read_char:
    push rdi
    push rsi
    push rdx
    call read_char
    pop rdx
    pop rsi
    pop rdi
    lea r8, [rax-9]
    cmp r8, 1 
    jbe .is_word_end
    cmp al, ' '
    je .is_word_end
    cmp rdx, rsi
    ja .too_long
    mov byte [rdi+rdx], al
    test al, al
    jz .word_end
    inc rdx
    jmp .read_char

.is_word_end:
    test rdx, rdx
    jz .read_char

.word_end:
    mov rax, rdi
    ret

.too_long:
    xor rax, rax
    ret


parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor rcx, rcx
.read_digit:
    mov cl, byte [rdi+rdx]
    xor cl, '0'
    cmp cl, 9
    ja .end
    lea rax, [rax*4 + rax]
    lea rax, [rax*2 + rcx]
    inc rdx
    jmp .read_digit
.end:
    ret



parse_int:
    mov cl, byte [rdi]
    cmp cl, ASCII_MINUS
    jne parse_uint
    inc rdi
    call parse_uint
    test rdx, rdx
    je .end
    inc rdx
    neg rax
.end:
    ret



string_copy:
    xor rax, rax 
    .copy_char:
        cmp rax, rdx
        jae .overflow

        mov cl, byte [rdi+rax]
        mov byte [rsi+rax], cl
        inc rax
        cmp cl, 0
        jne .copy_char
        ret
    .overflow:
        mov rax, 0
        ret
